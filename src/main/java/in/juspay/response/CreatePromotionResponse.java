package in.juspay.response;

import in.juspay.model.Promotion;
import in.juspay.model.PromotionCondition;
import in.juspay.model.PromotionStatus;

import java.util.List;

/**
 * Represents the response received when a promotion is applied against an order.
 */
public class CreatePromotionResponse {
    boolean success;
    Promotion promotion;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public Promotion getPromotion() {
        return promotion;
    }

    public void setPromotion(Promotion promotion) {
        this.promotion = promotion;
    }
}
