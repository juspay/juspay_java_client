package in.juspay.model;

public enum PromotionStatus {
    ACTIVE, INACTIVE
}
